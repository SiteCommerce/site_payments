<?php

namespace Drupal\site_payments;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for Bank account entities.
 *
 * @ingroup site_payments
 */
interface AccountInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the bank account creation timestamp.
   *
   * @return int
   *   Creation timestamp of bank account.
   */
  public function getCreatedTime(): int;

  /**
   * Sets bank account creation timestamp.
   *
   * @param int $timestamp
   *   bank account creation timestamp.
   *
   * @return \Drupal\site_payments\AccountInterface
   *   The called Bank account entity.
   */
  public function setCreatedTime($timestamp): AccountInterface;

  /**
   * Gets the payment plugin id.
   *
   * @return string
   *   The payment plugin id.
   */
  public function getPaymentPluginId(): string;

  /**
   * Gets the default vat type.
   *
   * @return string
   *   The default vat type.
   */
  public function getVatType(): string;

  /**
   * Gets payment plugins.
   *
   * @return array
   *   The payment plugins list.
   */
  public static function getPaymentPlugins(): array;
}
