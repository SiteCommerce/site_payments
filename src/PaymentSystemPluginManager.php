<?php

namespace Drupal\site_payments;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * PaymentSystemManager plugin manager.
 */
class PaymentSystemPluginManager extends DefaultPluginManager implements FallbackPluginManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/PaymentSystem', $namespaces, $module_handler, 'Drupal\site_payments\PaymentSystemPluginInterface', 'Drupal\site_payments\Annotation\PaymentSystem');
    $this->alterInfo('site_payments_payment_system_info');
    $this->setCacheBackend($cache_backend, 'site_payments_payment_system');
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'site_payments_bank_card';
  }
}
