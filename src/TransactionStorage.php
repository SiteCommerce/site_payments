<?php

namespace Drupal\site_payments;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\kvantstudio\Validator;

/**
 * Defines a storage class for Bank account entity.
 */
class TransactionStorage extends SqlContentEntityStorage implements TransactionStorageInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Implements data formatting functions.
   *
   * @var \Drupal\kvantstudio\Validator
   */
  protected $validator;

  /**
   * Constructs a CommentStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_info
   *   An array of entity info for the entity type.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend instance to use.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\kvantstudio\Validator $validator
   *   Implements data validating functions.
   */
  public function __construct(EntityTypeInterface $entity_info, Connection $database, EntityFieldManagerInterface $entity_field_manager, AccountInterface $current_user, CacheBackendInterface $cache, LanguageManagerInterface $language_manager, MemoryCacheInterface $memory_cache, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityTypeManagerInterface $entity_type_manager, Validator $validator) {
    parent::__construct($entity_info, $database, $entity_field_manager, $cache, $language_manager, $memory_cache, $entity_type_bundle_info, $entity_type_manager);
    $this->currentUser = $current_user;
    $this->validator = $validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_info) {
    return new static(
      $entity_info,
      $container->get('database'),
      $container->get('entity_field.manager'),
      $container->get('current_user'),
      $container->get('cache.entity'),
      $container->get('language_manager'),
      $container->get('entity.memory_cache'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager'),
      $container->get('kvantstudio.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createTransaction(array $data): TransactionInterface {
    // Get code of currency.
    $currency_code = $data['currency_code'] ?? NULL;
    if (!$currency_code || !preg_match('/^\p{Lu}{3}$/', $currency_code)) {
      $config = \Drupal::config('site_commerce_order.settings');
      $currency_code = empty($config->get('default_currency_code')) ? 'RUB' : $config->get('default_currency_code');
    }

    // Get default payment plugin.
    $payment_account_id = $data['payment_account'] ?? NULL;
    $payment_plugin_id = $data['payment_plugin'] ?? NULL;
    if (!$payment_account_id && $payment_plugin_id) {
      $plugin_service = \Drupal::service('plugin.manager.site_payments.payment_system');
      if ($plugin_service->hasDefinition($payment_plugin_id)) {
        $plugin_instance = $plugin_service->createInstance($payment_plugin_id);
        $payment_account = $plugin_instance->getAccount();
        if ($payment_account) {
          $payment_account_id = (int) $payment_account['id'];
        }
      }
    }

    /** @var \Drupal\site_payments\TransactionInterface $entity */
    $entity = $this->create([
      'uid' => (int) ($data['uid'] ?? $this->currentUser->id()),
      'google_uuid' => $data['google_uuid'] ?? NULL,
      'yandex_uuid' => $data['yandex_uuid'] ?? NULL,
      'client' => $data['client'] ?? NULL,
      'payment_account' => $payment_account_id,
      'payment_plugin' => $payment_plugin_id,
      'payment_system' => $data['payment_system'] ?? NULL,
      'payment_merchant_id' => $data['payment_merchant_id'] ?? NULL,
      'payment_id' => $data['payment_id'] ?? NULL,
      'payment_link' => $data['payment_link'] ?? NULL,
      'payment_status' => $data['payment_status'] ?? 'issued',
      'payment_type' => $data['payment_type'] ?? NULL,
      'order_id' => $data['order_id'] ?? NULL,
      'price' => [
        'number' => (float) ($data['price'] ?? 0),
        'currency_code' => $currency_code,
      ],
      'overdue' => $data['overdue'] ?? NULL,
      'webhook' => $data['webhook'] ?? NULL,
      'hostname' => $data['hostname'] ?? \Drupal::request()->getClientIp(),
      'description' => $data['description'] ?? NULL,
    ]);

    // Set UUID of transaction.
    if (!empty($data['uuid']) && $this->validator->isValidUuid($data['uuid'])) {
      $entity->set('uuid', $data['uuid']);
    }

    // Set user UUID of transaction.
    if (!empty($data['drupal_uuid']) && $this->validator->isValidUuid($data['drupal_uuid'])) {
      $entity->set('drupal_uuid', $data['drupal_uuid']);
    }

    // Created date of order.
    if (!empty($data['created']) && $created = (int) $data['created']) {
      $entity->set('created', $created);
    }

    // Changed date of order.
    if (!empty($data['changed']) && $changed = (int) $data['changed']) {
      $entity->set('changed', $changed);
    }

    // If additional data is passed as an array.
    if (!empty($data['data']) && is_array($data['data'])) {
      $entity->setData($data['data']);
    }

    $entity->enforceIsNew(TRUE);
    $entity->save();

    return $entity;
  }
}
