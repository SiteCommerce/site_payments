<?php

namespace Drupal\site_payments;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for Bank account entity storage class.
 */
interface AccountStorageInterface extends ContentEntityStorageInterface {

  /**
   * Returns the number of registered bank accounts.
   *
   * @param boolean $only_published
   * @return integer
   */
  public function countAccounts(bool $only_published = TRUE): int;

  /**
   * Resets the default selection flag for all bank accounts.
   *
   * @return void
   */
  public function resetDefaultAccounts();

  /**
   * Gets the payment plugin id.
   *
   * @param int|null $id
   *   The ID of payment account.
   *   If null return default payment plugin ID.
   *
   * @return string
   *   The payment plugin id.
   */
  public function getPaymentPluginId(?int $id = NULL): string;

}
