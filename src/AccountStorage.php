<?php

namespace Drupal\site_payments;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\kvantstudio\Formatter;

/**
 * Defines a storage class for Bank account entity.
 */
class AccountStorage extends SqlContentEntityStorage implements AccountStorageInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Implements data formatting functions.
   *
   * @var \Drupal\kvantstudio\Formatter
   */
  protected $formatter;

  /**
   * Constructs a CommentStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_info
   *   An array of entity info for the entity type.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend instance to use.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\kvantstudio\Formatter $formatter
   *   Implements data formatting functions.
   */
  public function __construct(EntityTypeInterface $entity_info, Connection $database, EntityFieldManagerInterface $entity_field_manager, AccountInterface $current_user, CacheBackendInterface $cache, LanguageManagerInterface $language_manager, MemoryCacheInterface $memory_cache, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityTypeManagerInterface $entity_type_manager, Formatter $formatter) {
    parent::__construct($entity_info, $database, $entity_field_manager, $cache, $language_manager, $memory_cache, $entity_type_bundle_info, $entity_type_manager);
    $this->currentUser = $current_user;
    $this->formatter = $formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_info) {
    return new static(
      $entity_info,
      $container->get('database'),
      $container->get('entity_field.manager'),
      $container->get('current_user'),
      $container->get('cache.entity'),
      $container->get('language_manager'),
      $container->get('entity.memory_cache'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager'),
      $container->get('kvantstudio.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function countAccounts(bool $only_published = FALSE): int {
    $query = $this->database->select('site_payments_account', 'n');
    $query->addExpression('COUNT(n.account_id)');

    if ($only_published) {
      $query->condition('n.status', 1);
    }

    return (int) $query->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function resetDefaultAccounts() {
    $query = $this->database->update('site_payments_account');
    $query->fields(['default' => 0]);
    $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentPluginId(?int $id = NULL): string {
    $query = $this->database->select('site_payments_account', 'n');
    $query->fields('n', ['payment_plugin']);

    if ($id) {
      $query->condition('n.account_id', $id);
    } else {
      $query->condition('n.default', 1);
    }

    return $query->execute()->fetchField();
  }
}
