<?php

namespace Drupal\site_payments;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Transaction entities.
 */
class TransactionViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }
}
