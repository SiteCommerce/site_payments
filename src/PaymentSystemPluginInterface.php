<?php

namespace Drupal\site_payments;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\site_payments\TransactionInterface;

interface PaymentSystemPluginInterface extends ContainerFactoryPluginInterface {

  /**
   * Метод, который возвращает ID плагина.
   */
  public function getId(): string;

  /**
   * Метод, который возвращает название плагина платежной системы.
   */
  public function getLabel(): string;

  /**
   * Метод, который возвращает название способа оплаты на странице оформления заказа.
   */
  public function getPaymentMethodName(): string;

  /**
   * Метод, который возвращает URL для перехода со страницы оплаты после успешной оплаты.
   */
  public function getSuccessUrl(string $uuid, string $route_name): string;

  /**
   * Метод, который возвращает URL для перехода со страницы оплаты после ошибки оплаты.
   */
  public function getFailureUrl(string $uuid, string $route_name): string;

  /**
   * Метод, который возвращает URL webhook для платежной системы.
   */
  public function getWebhookUrl(): string;

  /**
   * Метод, который возвращает объект с реквизитами подключения к платежной системе.
   *
   * @param integer|null $payment_account_id
   *   Идентификатор способа оплаты (ID сущности банковского счета).
   * @return array
   */
  public function getPaymentSettings(?int $payment_account_id = NULL): array;

  /**
   * Метод, который возвращает TRUE - если способ оплаты доступен для выбора пользователю.
   */
  public function getAvailableStatus(): bool;

  /**
   * Метод, который возвращает ссылку на оплату.
   */
  public function getPaymentUrl(array $payment_data): string;

  /**
   * Метод, который регистрирует транзакцию.
   */
  public function createTransaction(array $payment_data, bool $get_transaction = FALSE): TransactionInterface|int|false;

  /**
   * Метод, который проверяет в платежной системе статус оплаты и возвращает его название.
   *
   * @param \Drupal\site_payments\TransactionInterface $transaction
   *   Транзакция.
   * @param array $settings
   *   Дополнительные настройки, которые заменяют или расширяют настройки платежного плагина.
   *
   * @return string|null
   *   Название статуса оплаты по классификатору на сайте.
   */
  public function checkPaymentStatus(TransactionInterface $transaction, array $settings = []): ?string;

  /**
   * Метод, который возвращает правила платежной системы.
   */
  public function getRulesPaymentSystem(): array;

  /**
   * Метод, который проверяет возможность выполнения оплаты.
   * Например при оплате с баланса в личном кабинете.
   * Средств может быть не достаточно для оплаты.
   *
   * Функция возвращает TRUE - если достаточно баланса.
   * Для плагинов онлайн оплаты и наличные деньги всегда нужно возвращать TRUE.
   */
  public function checkUserBalance(float $order_cost = 0): bool;

  /**
   * Метод, который обновляет при оформлении заказа баланс в платежной системе.
   * Например при оплате с баланса в личном кабинете.
   *
   * @param array $payment_data
   *   Данные необходимые для обновления баланса в личном кабинете на сайте.
   * @param string $type
   *   write_off - списание, refill - начисление.
   */
  public function updateUserBalance(array $payment_data, string $type = 'write_off'): bool;

  /**
   * Метод, который возвращает массив с данными о настройке банковского счета
   * для текущей платежной системы.
   *
   * @param int|null $payment_account_id
   *   Идентификатор счета для загрузки. Если NULL, то будет выбран счет по умолчанию.
   *
   * @return array|null
   */
  public function getAccount(?int $payment_account_id = NULL): ?array;

  /**
   * Get order id for payment system.
   *
   * @param \Drupal\site_payments\TransactionInterface $transaction
   *   Transaction.
   *
   * @return integer|string
   */
  public function getOrderId(TransactionInterface $transaction): int|string;

  /**
   * Метод создает сущность счета на оплату для ОФД.
   * Для каждой платежной системы нужно проверять
   * достаточно ли параметров для чека ОФД и при необходимости
   * переопределять метод в плагине платежной системы.
   *
   * @param TransactionInterface $transaction
   * @return ReceiptInterface
   */
  public function createReceipt(TransactionInterface $transaction): ReceiptInterface;

  /**
   * Метод, который создает чек на оплату в ОФД.
   */
  public function sendReceipt(ReceiptInterface $receipt): ?array;

  /**
   * Метод, который проверяет состояние чека на оплату в ОФД.
   */
  public function checkReceiptStatus(ReceiptInterface $receipt): ?array;

  /**
   * Метод, который изменяет стоимость заказа до минимально возможной
   * если пользователю разрешено тестирование заказов.
   * Для корректной передачи данных в ОФД фактически выбранные позиции
   * в корзине заменяются тестовой позицией с минимальной ценой.
   */
  public function correctPaidNumber(TransactionInterface $transaction, array $config): float;

  /**
   * Метод возвращает значение ставки НДС для платежной системы.
   * Каждая платежная система имеет свой внутренний кодификатор ставок НДС в виде числа или текста.
   *
   * @param string|null $type
   *   Значение ставки НДС у позиции корзины.
   *   Например:
   *     пустая строка - ставка НДС должна браться из настроек банковского счета $default_type.
   *     NONE - без НДС.
   *     0 - ставка НДС 0%.
   *     20 - ставка НДС 20%.
   *     120 — ставка НДС расч. 20/120. и т.п.
   * @param string $default_type
   *   Cтавка НДС из настроек банковского счета.
   * @return int|string
   */
  public function getVatType(?string $type, string $default_type = 'NONE'): int|string;

}
