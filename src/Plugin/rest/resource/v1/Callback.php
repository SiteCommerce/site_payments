<?php

namespace Drupal\site_payments\Plugin\rest\resource\v1;

use Drupal\Component\Serialization\Json;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Регистрирует ответ от платежной системы после оплаты.
 * Принимает данные и перенаправляет их в платежный плагин
 * для дальнейшей обработки.
 * TODO: метод в разработке.
 *
 * @RestResource(
 *   id = "site_payments_callback",
 *   label = @Translation("Payments: callback for payment system"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/site-payments/callback",
 *     "create" = "/api/v1/site-payments/callback",
 *   }
 * )
 */
final class Callback extends ResourceBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * Responds to GET requests.
   */
  public function get(Request $request) {
    $response['result'] = NULL;

    try {
      $parametrs = $request->query->all();
      $parametrs= Json::encode($parametrs);
      $this->logger->info($parametrs);

      return new ModifiedResourceResponse($response);
    } catch (\Exception $e) {
      return new ModifiedResourceResponse('Something went wrong.', 404);
    }

    return new ModifiedResourceResponse($response);
  }

  /**
   * Responds to POST requests.
   */
  public function post(?array $data = []) {
    $response['result'] = NULL;

    try {
      $payment = file_get_contents('php://input');
      $payment = Json::encode($payment);
      $this->logger->info($payment);

      return new ModifiedResourceResponse($response);
    } catch (\Exception $e) {
      return new ModifiedResourceResponse('Something went wrong. Check your data.', 400);
    }
  }
}
