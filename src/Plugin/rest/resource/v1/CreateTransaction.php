<?php

namespace Drupal\site_payments\Plugin\rest\resource\v1;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\site_payments\TransactionInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Регистрирует транзакцию платежной системы.
 *
 * @RestResource(
 *   id = "site_payments_create_transaction",
 *   label = @Translation("Payments: create transaction"),
 *   uri_paths = {
 *     "create" = "/api/v1/site-payments/create-transaction",
 *   }
 * )
 */
final class CreateTransaction extends ResourceBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * Responds to POST requests.
   */
  public function post($data) {
    $response['result'] = NULL;

    try {
      // Проверка существования транзакции для заказа.
      $order_id = (int) $data['order_id'];

      $result = $this->entityTypeManager->getStorage('site_payments_transaction')->loadByProperties(['order_id' => $order_id]);
      $transaction = reset($result);
      if ($transaction instanceof TransactionInterface) {
        $response['status'] = FALSE;
        $response['message'] = $this->t('The order number № @value has already been registered.', ['@value' => $order_id]);
        return new ModifiedResourceResponse($response);
      }

      // Проверка существования покупателя в списке зарегистрированных пользователей.
      $result = $this->entityTypeManager->getStorage('user')->loadByProperties(['name' => $data['name']]);
      $account = reset($result);
      if ($account instanceof User) {
        $data['uid'] = $account->id();
      } else {
        $response['status'] = FALSE;
        $response['message'] = $this->t('Account @name does not exist.', ['@name' => $data['name']]);
        return new ModifiedResourceResponse($response);
      }

      $transaction = \Drupal::service('site_payments.transaction')->createTransaction($data);
      if ($transaction instanceof TransactionInterface) {
        $response['result']['transaction_id'] = $transaction->id();
        $response['status'] = TRUE;
      } else {
        $response['status'] = FALSE;
      }

      return new ModifiedResourceResponse($response);
    } catch (\Exception $e) {
      return new ModifiedResourceResponse('Something went wrong. Check your data.', 400);
    }
  }
}
