<?php

/**
 * @file
 * Contains \Drupal\site_payments\Plugin\Field\FieldFormatter\PaymentMethod.
 */

namespace Drupal\site_payments\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the payment method formatter.
 *
 * @FieldFormatter(
 *   id = "site_payments_payment_method",
 *   label = @Translation("Payment method"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class PaymentMethod extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $plugin_service = \Drupal::service('plugin.manager.site_payments.payment_system');

    foreach ($items as $delta => $item) {
      $instance = $plugin_service->createInstance($item->value);
      $elements[$delta] = [
        '#markup' => $instance->getPaymentMethodName(),
      ];
    }

    return $elements;
  }

}
