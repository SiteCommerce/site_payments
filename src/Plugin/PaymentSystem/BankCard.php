<?php

namespace Drupal\site_payments\Plugin\PaymentSystem;

use Drupal\site_payments\PaymentSystemPluginBase;

/**
 * @PaymentSystem(
 *   id="site_payments_bank_card",
 *   label = @Translation("Bank card"),
 *   payment_method_name = @Translation("Bank card upon receipt")
 * )
 */
class BankCard extends PaymentSystemPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPaymentSettings(?int $payment_account_id = NULL): array {
    $config = $this->configFactory->get('site_payments.bank_card.settings');
    return [
      'allow_select' => (bool) $config->get('allow_select'),
      'payment_method_name' => $config->get('payment_method_name'),
      'block_rules_payment_system' => (int) $config->get('block_rules_payment_system'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentMethodName(): string {
    $settings = $this->getPaymentSettings();
    $payment_method_name = empty($settings['payment_method_name']) ? $this->pluginDefinition['payment_method_name'] : $settings['payment_method_name'];

    return $payment_method_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableStatus(): bool {
    $settings = $this->getPaymentSettings();

    return (bool) $settings['allow_select'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRulesPaymentSystem(): array {
    $settings = $this->getPaymentSettings();

    $build = [];
    if ($settings['block_rules_payment_system']) {
      $entity_type = 'block_content';
      $view_mode = 'default';
      $block = $this->entityTypeManager->getStorage($entity_type)->load($settings['block_rules_payment_system']);
      $view_builder = $this->entityTypeManager->getViewBuilder($entity_type);
      $build = $view_builder->view($block, $view_mode);
    }

    return $build;
  }

}
