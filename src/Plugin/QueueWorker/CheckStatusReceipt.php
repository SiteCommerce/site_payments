<?php

declare(strict_types=1);

namespace Drupal\site_payments\Plugin\QueueWorker;

use Drupal\Core\Utility\Error;
use Drupal\Core\Annotation\QueueWorker;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\site_payments\ReceiptInterface;

/**
 * Check status of receipt.
 *
 * @QueueWorker(
 *   id = "site_payments_check_status_receipt",
 *   title = @Translation("Check status of receipt"),
 *   cron = {"time" = 60},
 *   weight = 0
 * )
 */
class CheckStatusReceipt extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    try {
      $receipt_id = (int) $data['receipt_id'];

      /** @var \Drupal\site_payments\ReceiptInterface $receipt */
      $receipt = $this->entityTypeManager->getStorage('site_payments_receipt')->load($receipt_id);
      if ($receipt instanceof ReceiptInterface) {
        $transaction = $receipt->getTransaction();

        $plugin_service = \Drupal::service('plugin.manager.site_payments.payment_system');
        $payment_plugin_id = $transaction->getPaymentPluginId();
        if ($plugin_service->hasDefinition($payment_plugin_id)) {
          $plugin_instance = $plugin_service->createInstance($payment_plugin_id);
          $plugin_instance->checkReceiptStatus($receipt);
        }
      }
    } catch (\Exception $e) {
      Error::logException(\Drupal::logger('site_payments'), $e);
    }
  }
}
