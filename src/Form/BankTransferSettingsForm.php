<?php

namespace Drupal\site_payments\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class Form of Bank transfer payment plugin settings.
 */
class BankTransferSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'site_payments.bank_transfer.settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'site_payments_bank_transfer_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config(static::SETTINGS);

    // Настройки.
    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
    ];

    // Разрешить выбор способа оплаты.
    $form['settings']['allow_select'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow selection of payment method'),
      '#default_value' => $config->get('allow_select'),
    ];

    // Название способа оплаты.
    $form['settings']['payment_method_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment method name'),
      '#default_value' => $config->get('payment_method_name'),
      '#description' => $this->t('If the field is empty, the name of the payment method will be loaded from the properties of the payment plugin.')
    ];

    $block_rules_payment_system = (int) $config->get('block_rules_payment_system');
    $block = \Drupal::entityTypeManager()->getStorage('block_content')->load($block_rules_payment_system);
    $form['block_rules_payment_system'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Rules for the use of the payment system'),
      '#target_type' => 'block_content',
      '#default_value' => $block,
      '#description' => $this->t('You need to create a custom block and select it in this field. The block content is displayed under the select payment method field.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Записывает значения в конфигурацию.
    $config = $this->configFactory->getEditable(static::SETTINGS);
    $config->set('allow_select', (int) $form_state->getValue('allow_select'));
    $config->set('payment_method_name', trim($form_state->getValue('payment_method_name')));
    $config->set('block_rules_payment_system', (int) $form_state->getValue('block_rules_payment_system'));
    $config->save();
  }
}
