<?php

namespace Drupal\site_payments\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Global payments settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The Sate API object.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a \Drupal\site_payments\Form\SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $state) {
    parent::__construct($config_factory);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'site_payments_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'site_payments.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['allow_test_payment'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow order payment testing'),
      '#default_value' => $this->state->get('site_payments.allow_test_payment')
    ];

    $form['test_hostnames'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IP addresses for which order payment testing is allowed'),
      '#default_value' => $this->state->get('site_payments.test_hostnames'),
      '#description' => $this->t("The values must be separated by ';'."),
      '#states' => [
        'visible' => [
          ':input[name="allow_test_payment"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['test_usernames'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Usernames for which order payment testing is allowed'),
      '#default_value' => $this->state->get('site_payments.test_usernames'),
      '#description' => $this->t("The values must be separated by ';'."),
      '#states' => [
        'visible' => [
          ':input[name="allow_test_payment"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $allow_test_payment = (bool) $form_state->getValue('allow_test_payment');
    $this->state->set('site_payments.allow_test_payment', $allow_test_payment);

    $test_hostnames = trim($form_state->getValue('test_hostnames'));
    $this->state->set('site_payments.test_hostnames', $test_hostnames);

    $test_usernames = trim($form_state->getValue('test_usernames'));
    $this->state->set('site_payments.test_usernames', $test_usernames);
  }
}
