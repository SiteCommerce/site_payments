<?php

namespace Drupal\site_payments\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Bank account entities.
 *
 * @ingroup site_payments
 */
class AccountDeleteForm extends ContentEntityDeleteForm {
}
