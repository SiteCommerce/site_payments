<?php

namespace Drupal\site_payments;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for 'site_payments_transaction' entity storage class.
 */
interface TransactionStorageInterface extends ContentEntityStorageInterface {

  /**
   * Helper function for create transaction.
   *
   * @param array $data
   *   Array with parametrs for create transaction.
   *
   * @return \Drupal\site_payments\TransactionInterface
   */
  public function createTransaction(array $data): TransactionInterface;

}
