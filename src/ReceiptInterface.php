<?php

namespace Drupal\site_payments;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for Receipt entities.
 *
 * @ingroup site_payments
 */
interface ReceiptInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the creation timestamp.
   *
   * @return int
   *   Creation timestamp.
   */
  public function getCreatedTime(): int;

  /**
   * Sets creation timestamp.
   *
   * @param int $timestamp
   *   Creation timestamp.
   *
   * @return \Drupal\site_payments\ReceiptInterface
   *   The called Receipt entity.
   */
  public function setCreatedTime($timestamp): ReceiptInterface;

  /**
   * Gets additional data.
   *
   * @return array|null
   */
  public function getData(): array|null;

  /**
   * Sets additional data.
   *
   * @param array $data
   *   The additional data.
   *
   * @return \Drupal\site_payments\Entity\ReceiptInterface
   *   The called entity.
   */
  public function setData(array $data): ReceiptInterface;

  /**
   * Gets transaction entity.
   *
   * @return Drupal\site_payments\TransactionInterface
   */
  public function getTransaction(): TransactionInterface;

  /**
   * Gets status.
   *
   * @return string. Available statuses: "FAILED SEND", "NEW", "IN_PROGRESS", "DONE", "FAILED","AWAITING".
   */
  public function getStatus(): string;

  /**
   * Sets status.
   *
   * @param string $status
   *   The status.
   *
   * @return \Drupal\site_payments\Entity\ReceiptInterface
   *   The called entity.
   */
  public function setStatus(string $status): ReceiptInterface;

}
