<?php

namespace Drupal\site_payments;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for Bank account entity.
 *
 * @see \Drupal\site_payments\Entity\Account.
 */
class AccountAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\site_payments\AccountInterface $entity */

    switch ($operation) {
      case 'edit':
        return AccessResult::allowedIfHasPermission($account, 'administer site_payments_account');
      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'administer site_payments_account');
    }

    // Unknown operation, no opinion.
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'administer site_payments_account');
  }
}
