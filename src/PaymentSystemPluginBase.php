<?php

namespace Drupal\site_payments;

use Drupal\Component\Plugin\PluginBase;
use Drupal\site_payments\TransactionInterface;
use Drupal\Core\Url;
use Drupal\site_payments\Event\PaymentStatusEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Utility\Error;

/** @package Drupal\site_payments */
abstract class PaymentSystemPluginBase extends PluginBase implements PaymentSystemPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Sate API object.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The event dispatcher to dispatch the filename sanitize event.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->configFactory = $container->get('config.factory');
    $instance->logger = $container->get('logger.factory')->get('site_payments');
    $instance->state = $container->get('state');
    $instance->eventDispatcher = $container->get('event_dispatcher');
    $instance->messenger = $container->get('messenger');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentMethodName(): string {
    return $this->pluginDefinition['payment_method_name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableStatus(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function checkUserBalance(float $order_cost = 0): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateUserBalance(array $payment_data, string $type = 'write_off'): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSuccessUrl(string $uuid, string $route_name = 'site_payments.payment_success'): string {
    $url = Url::fromRoute($route_name, ['uuid' => $uuid], ['absolute' => TRUE]);
    return $url->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function getFailureUrl(string $uuid, string $route_name = 'site_payments.payment_failure'): string {
    $url = Url::fromRoute($route_name, ['uuid' => $uuid], ['absolute' => TRUE]);
    return $url->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentUrl(array $payment_data): string {
    // We determine which bank account to choose in the payment system.
    $payment_account_id = empty($payment_data['payment_account']) ? NULL : (int) $payment_data['payment_account'];

    // Payment system settings for a specific bank account.
    $config = $this->getPaymentSettings($payment_account_id);

    // Payment system settings for a specific bank account.
    $payment_data['payment_account'] = NULL;

    // Expiration time of the payment link.
    $payment_data['overdue'] = NULL;

    // The name of the payment plugin.
    $payment_data['payment_plugin'] = $this->getId();

    // The name of the payment system.
    $payment_data['payment_type'] = $payment_data['payment_type'] ?? 'offline';

    // The name of the payment system.
    $payment_data['payment_system'] = NULL;

    // Register a transaction.
    $transaction = $this->createTransaction($payment_data, TRUE);
    if ($transaction instanceof TransactionInterface) {
      // Change the price for transaction.
      $payment_data['price'] = $this->correctPaidNumber($transaction, $config);

      // Register receipt.
      $this->createReceipt($transaction);

      $url = Url::fromRoute('site_commerce_order.confirmed', ['order_uuid' => $payment_data['order_uuid']]);
    } else {
      $url = Url::fromRoute('<front>')->setAbsolute();
      $this->messenger->addError(t('There was an error when placing your order.'));

      $error_data = json_encode($payment_data, JSON_UNESCAPED_UNICODE);
      $this->logger->error("Transaction not created: {$error_data}.");
    }

    return $url->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function getWebhookUrl(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function createTransaction(array $data, bool $get_transaction = FALSE): TransactionInterface|int|false {
    /** @var \Drupal\site_payments\TransactionStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('site_payments_transaction');
    $transaction = $storage->createTransaction($data);
    if ($transaction instanceof TransactionInterface) {
      return $get_transaction ? $transaction : (int) $transaction->id();
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function checkPaymentStatus(TransactionInterface $transaction, array $settings = []): ?string {
    $result = NULL;

    // Create params for query to payment system.
    $params = [
      'order_id' => $this->getOrderId($transaction),
      'status' => $transaction->getPaymentStatus(),
      'result' => NULL
    ];

    // Dispatch event CHECK_PAYMENT_STATUS_BEFORE.
    $event = new PaymentStatusEvent($transaction, $params);
    $this->eventDispatcher->dispatch($event, PaymentStatusEvent::CHECK_PAYMENT_STATUS_BEFORE);

    // Reload params.
    $transaction = $event->getTransaction();
    $params = $event->getData();

    // Check status from API.
    $result = $params['status'];

    // Dispatch event PAYMENT_STATUS_PAID.
    $event = new PaymentStatusEvent($transaction, $params);
    $this->eventDispatcher->dispatch($event, PaymentStatusEvent::PAYMENT_STATUS_PAID);
    $params = $event->getData();

    // Some other functions.

    // Dispatch event CHECK_PAYMENT_STATUS_AFTER.
    $event = new PaymentStatusEvent($transaction, $params);
    $this->eventDispatcher->dispatch($event, PaymentStatusEvent::CHECK_PAYMENT_STATUS_AFTER);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccount(?int $payment_account_id = NULL): ?array {
    $account = NULL;

    /** @var \Drupal\site_payments\AccountStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('site_payments_account');

    if ($payment_account_id) {
      $entities = $storage->loadByProperties([
        'payment_plugin' => $this->getId(),
        'account_id' => $payment_account_id
      ]);
    } else {
      $entities = $storage->loadByProperties([
        'payment_plugin' => $this->getId(),
        'default' => 1
      ]);
    }

    /** @var \Drupal\site_payments\AccountInterface $entity */
    $entity = $entities ? reset($entities) : FALSE;
    if ($entity) {
      $account = [
        'id' => (int) $entity->id(),
        'uuid' => $entity->uuid(),
        'title' => $entity->label(),
        'uid' => $entity->getOwnerId(),
        'payment_plugin' => $this->getId(),
        'mode' => (bool) $entity->get('mode')->value,
        'verify_ssl' => (bool) $entity->get('verify_ssl')->value,
        'login' => $entity->get('login')->value,
        'password' => $entity->get('password')->value,
        'token' => $entity->get('token')->value,
        'merchant' => $entity->get('merchant')->value,
        'payment_system' => $entity->get('payment_system')->value,
        'minimum_order_price' => (int) $entity->get('minimum_order_price')->value,
        'overdue' => (int) $entity->get('overdue')->value,
        'default' => (bool) $entity->get('default')->value,
        'fiscal' => $entity->get('fiscal')->value,
        'vat_type' => (string) $entity->get('vat_type')->value,
        'status' => (bool) $entity->get('status')->value,
        'created' => (int) $entity->get('created')->value,
        'changed' => (int) $entity->get('changed')->value,
      ];
    }

    return $account;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderId(TransactionInterface $transaction): int|string {
    return $transaction->uuid();
  }

  /**
   * {@inheritdoc}
   */
  public function getVatType(?string $type, string $default_type = 'NONE'): int|string {
    return $type ?? $default_type;
  }

  /**
   * {@inheritdoc}
   */
  public function createReceipt(TransactionInterface $transaction): ReceiptInterface {
    $storage_receipt = $this->entityTypeManager->getStorage('site_payments_receipt');
    $receipts = $storage_receipt->loadByProperties([
      'transaction_id' => $transaction->id()
    ]);
    $receipt = $receipts ? reset($receipts) : FALSE;

    if (!$receipt) {
      try {
        $payment_account = $transaction->getPaymentAccount();
        $default_vat_type = $payment_account ? $payment_account->getVatType() : $this->getVatType(NULL);

        /** @var \Drupal\site_commerce_order\OrderInterface $order */
        $order = $this->entityTypeManager->getStorage('site_commerce_order')->load($transaction->getOrderId());
        $cart_items = $this->entityTypeManager->getStorage('site_commerce_cart')->loadByProperties(['order_id' => $transaction->getOrderId()]);
        $items = [];
        /** @var \Drupal\site_commerce_cart\CartInterface $cart */
        foreach ($cart_items as $cart) {
          $vat_type = $this->getVatType($cart->getVatType(), $default_vat_type);
          $items[] = [
            'name' => $cart->getReceiptTitle(),
            'price' => $cart->getPaid(TRUE),
            'quantity' => $cart->getQuantity(),
            'amount' => $cart->getTotalPaid(TRUE),
            'vatType' => $vat_type,
            'paymentObject' => $cart->getPaymentObject(),
            'paymentMode' => $cart->getPaymentMode(),
            'measurementUnit' => $cart->getMeasurementUnit()
          ];
        }

        /** @var \Drupal\site_payments\ReceiptInterface $receipt */
        $receipt = $storage_receipt->create([
          'transaction_id' => $transaction->id(),
          'status' => 'CREATED',
        ]);

        $receipt->setData([
          'receiptNumber' => $receipt->uuid(),
          'client' => [
            'email' => $order->getCustomerMail()
          ],
          'items' => $items,
          'total' => $transaction->getPrice(TRUE)
        ]);

        $receipt->save();
      } catch (\Exception $exception) {
        Error::logException($this->logger, $exception);
      }
    }

    return $receipt;
  }

  /**
   * {@inheritdoc}
   */
  public function sendReceipt(ReceiptInterface $receipt): ?array {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function checkReceiptStatus(ReceiptInterface $receipt): ?array {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function correctPaidNumber(TransactionInterface $transaction, array $config): float {
    $total = $transaction->getPrice();

    // Change the price for transaction.
    $allow_test_payment = (bool) ($config['allow_test_payment'] ?? FALSE);
    if ($allow_test_payment) {
      $is_change = FALSE;

      // Change the price for test IP addresses.
      $test_hostnames = $config['test_hostnames'];
      if ($test_hostnames) {
        $hostnames = explode(";", $test_hostnames);
        $ip = \Drupal::request()->getClientIp();
        if (in_array($ip, $hostnames)) {
          $total = (float) ($config['minimum_order_price'] ?? 1);
          $is_change = TRUE;
        }
      }

      // Change the price for test usernames.
      $test_usernames = $config['test_usernames'];
      if ($test_usernames) {
        $usernames = explode(";", $test_usernames);
        $owner_id = $transaction->getOwnerId();
        foreach ($usernames as $username) {
          $account = user_load_by_name($username);
          if ($account && $account->id() == $owner_id) {
            $total = (float) ($config['minimum_order_price'] ?? 1);
            $is_change = TRUE;
          }
        }
      }

      if ($is_change) {
        $order_id = $transaction->getOrderId();

        /** @var \Drupal\site_commerce_cart\CartStorageInterface $storage_cart */
        $storage_cart = $this->entityTypeManager->getStorage('site_commerce_cart');
        $cart_items = $storage_cart->loadByProperties(['order_id' => $order_id]);
        if ($cart_items) {
          $count_items = count($cart_items);
          $price = ceil($total / $count_items);

          /** @var \Drupal\site_commerce_cart\CartInterface $cart_item */
          foreach ($cart_items as $cart_item) {
            $cart_item->setQuantity(1)->setPrice($price)->setPaid($price)->save();
          }

          $total = (float) ($price * $count_items);

          /** @var \Drupal\site_commerce_order\OrderInterface $order */
          $order = $this->entityTypeManager->getStorage('site_commerce_order')->load($order_id);
          $order->setPrice($total)->setPaid($total)->save();

          $transaction->setPrice($total)->save();
        }
      }
    }

    return $total;
  }
}
