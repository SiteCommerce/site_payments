<?php

namespace Drupal\site_payments\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\site_payments\AccountInterface;

/**
 * Defines the Bank account entity.
 *
 * @ingroup site_payments
 *
 * @ContentEntityType(
 *   id = "site_payments_account",
 *   label = @Translation("Bank account"),
 *   label_collection = @Translation("Bank accounts"),
 *   label_singular = @Translation("bank account"),
 *   label_plural = @Translation("bank accounts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count bank account",
 *     plural = "@count bank accounts",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\site_payments\AccountStorage",
 *     "list_builder" = "Drupal\site_payments\AccountListBuilder",
 *     "access" = "Drupal\site_payments\AccountAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\site_payments\Form\AccountForm",
 *       "add" = "Drupal\site_payments\Form\AccountForm",
 *       "edit" = "Drupal\site_payments\Form\AccountForm",
 *       "delete" = "Drupal\site_payments\Form\AccountDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\site_payments\AccountHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "site_payments_account",
 *   admin_permission = "administer site_payments_account",
 *   fieldable = FALSE,
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "account_id",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "label" = "title",
 *   },
 *   links = {
 *     "add-form" = "/admin/site-commerce/config/payment-systems/accounts/add",
 *     "edit-form" = "/admin/site-commerce/config/payment-systems/accounts/{site_payments_account}/edit",
 *     "delete-form" = "/admin/site-commerce/config/payment-systems/accounts/{site_payments_account}/delete",
 *     "collection" = "/admin/site-commerce/config/payment-systems/bank-accounts",
 *   }
 * )
 */
class Account extends ContentEntityBase implements AccountInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return (int) $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp): AccountInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getVatType(): string {
    return (string) $this->get('vat_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentPluginId(): string {
    return (string) $this->get('payment_plugin')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): ?int {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): AccountInterface {
    $this->set('uid', $uid);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): AccountInterface {
    $this->set('uid', $account->id());

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');

    $fields['payment_plugin'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Payment system'))
      ->setSetting('allowed_values_function', ['\Drupal\site_payments\Entity\Account', 'getPaymentPlugins'])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 0,
        'multiple_values' => FALSE
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['mode'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(t('Operating mode'))
      ->setSettings([
        'allowed_values' => [
          0 => t('Test mode'),
          1 => t('Combat mode'),
        ],
        'size' => 'tiny',
        'unsigned', TRUE,
      ])
      ->setDefaultValue(0)
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 0,
        'multiple_values' => FALSE
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['verify_ssl'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(t('Verify the SSL certificate'))
      ->setSettings([
        'allowed_values' => [
          1 => t('Yes'),
          0 => t('No'),
        ],
        'size' => 'tiny',
        'unsigned', TRUE,
      ])
      ->setDefaultValue(1)
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 0,
        'multiple_values' => FALSE
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['fiscal'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('The method of sending fiscal data'))
      ->setSettings([
        'allowed_values' => [
          'default' => t('By default'),
          'registration' => t('Transfer of fiscal information during order registration'),
        ],
      ])
      ->setDefaultValue('default')
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 0,
        'multiple_values' => FALSE
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['vat_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('The tax system'))
      ->setSettings([
        'allowed_values' => [
          'NONE' => t('VAT is not subject to'),
          '0' => t('The VAT rate is 0%'),
          '5' => t('The VAT rate is 5%'),
          '7' => t('The VAT rate is 7%'),
          '10' => t('The VAT rate is 10%'),
          '20' => t('The VAT rate is 20%'),
        ],
      ])
      ->setDefaultValue('NONE')
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 0,
        'multiple_values' => FALSE
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Short name of the bank account'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['login'] = BaseFieldDefinition::create('string')
      ->setLabel(t('User name'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['password'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Password'))
      ->setSetting('max_length', 1024)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['token'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Secret key'))
      ->setSetting('max_length', 1024)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['merchant'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Merchant ID'))
      ->setSetting('max_length', 1024)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['payment_system'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ID of the payment method'))
      ->setDescription(t('ID of the payment method in the financial accounting system (for example, in 1C).'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['minimum_order_price'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Minimum order price'))
      ->setDescription(t('The value is used when testing payment for orders. Depends on the rules of the payment system.'))
      ->setRequired(TRUE)
      ->setDefaultValue(1)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['overdue'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('The time (in hours) when the link to the payment will go bad'))
      ->setRequired(TRUE)
      ->setSetting('unsigned', TRUE)
      ->setDefaultValue(24)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['default'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('The default account for crediting funds'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 0,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Allow selection of payment method'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 0,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('Date the item was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('Date the item was edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function getPaymentPlugins(): array {
    $payment_methods = [];

    $exclude = ['site_payments_cash', 'site_payments_bank_transfer'];

    if (\Drupal::hasService('plugin.manager.site_payments.payment_system')) {
      $plugin_service = \Drupal::service('plugin.manager.site_payments.payment_system');
      foreach ($plugin_service->getDefinitions() as $plugin_id => $plugin) {
        if (!in_array($plugin_id, $exclude)) {
          $instance = $plugin_service->createInstance($plugin_id);
          $payment_methods[$plugin_id] = $instance->getPaymentMethodName();
        }
      }
    }

    return $payment_methods;
  }
}
