<?php

namespace Drupal\site_payments\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\UserInterface;
use Drupal\kvantstudio\Formatter;
use Drupal\site_payments\TransactionInterface;

/**
 * Defines the Transaction entity.
 *
 * @ingroup site_payments
 *
 * @ContentEntityType(
 *   id = "site_payments_transaction",
 *   label = @Translation("Transaction"),
 *   handlers = {
 *     "storage" = "Drupal\site_payments\TransactionStorage",
 *     "views_data" = "Drupal\site_payments\TransactionViewsData",
 *     "access" = "Drupal\site_payments\TransactionAccessControlHandler",
 *   },
 *   base_table = "site_payments_transaction",
 *   admin_permission = "administer site_payments",
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "transaction_id",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *   }
 * )
 */
class Transaction extends ContentEntityBase implements TransactionInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public function isNewClient() {
    return (bool) $this->get('client')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrice(bool $format = FALSE) {
    $number = $this->get('price')->number;
    if ($format) {
      $number = Formatter::removeTrailingZeros($number);
    }

    return (float) $number;
  }

  /**
   * {@inheritdoc}
   */
  public function setPrice(float $value, string $currency_code = NULL) {
    $this->set('price', [
      'number' => $value,
      'currency_code' => $currency_code ?: $this->getPriceСurrencyСode()
    ]);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPriceСurrencyСode() {
    return $this->get('price')->currency_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentPluginId() {
    return $this->get('payment_plugin')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentSystemId() {
    return $this->get('payment_system')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentId() {
    return $this->get('payment_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPaymentId(?string $payment_id = NULL) {
    $this->set('payment_id', $payment_id);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentType() {
    return $this->get('payment_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentAccountId() {
    return (int) $this->get('payment_account')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentAccount() {
    return $this->get('payment_account')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return (int) $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp) {
    $this->set('created', $timestamp);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOverdueTime() {
    return (int) $this->get('overdue')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setOverdueTime(int $timestamp) {
    $this->set('overdue', $timestamp);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPaymentOverdue(?int $request_time = NULL) {
    if (!$request_time) {
      $request_time = \Drupal::time()->getRequestTime();
    }

    $overdue = (int) $this->get('overdue')->value;
    if ($overdue) {
      return ($overdue > $request_time) ? FALSE : TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentStatus() {
    return $this->get('payment_status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPaymentStatus(string $payment_status) {
    $this->set('payment_status', $payment_status);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPaid() {
    $payment_status = $this->get('payment_status')->value;
    if ($payment_status == 'paid') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentLink() {
    return $this->get('payment_link')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPaymentLink(?string $payment_link = NULL) {
    $this->set('payment_link', $payment_link);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFinanceSystemStatus() {
    return (bool) $this->get('finance_system')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setFinanceSystemStatus(bool $status = TRUE) {
    $timestamp = $status ? \Drupal::time()->getRequestTime() : NULL;
    $this->set('finance_system', $timestamp);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWebhookStatus() {
    return (bool) $this->get('webhook')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setWebhookStatus(bool $status = TRUE) {
    $timestamp = $status ? \Drupal::time()->getRequestTime() : NULL;
    $this->set('webhook', $timestamp);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getViewedTime() {
    return (int) $this->get('viewed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setViewedTime(int $timestamp) {
    $this->set('viewed', $timestamp);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $data = NULL;
    $value = $this->get('data')->getValue();
    if (!empty($value)) {
      $data = reset($value) ?: NULL;
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function setData(array $data) {
    $this->set('data', $data);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderId() {
    return (int) $this->get('order_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrderId(int $order_id) {
    $this->set('order_id', $order_id);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription(string $description) {
    $this->set('description', $description);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('User'))
      ->setDescription(new TranslatableMarkup('User, owner of the transaction.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');

    $fields['drupal_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('User UUID'))
      ->setRequired(FALSE);

    $fields['google_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('User ID Google'))
      ->setRequired(FALSE);

    $fields['yandex_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('User ID Yandex'))
      ->setRequired(FALSE);

    $fields['client'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Type of client'))
      ->setDescription(new TranslatableMarkup('The status of the order being made by a new customer who has not previously made a purchase: 1 - the customer is new.'))
      ->setRequired(FALSE)
      ->setSettings([
        'size' => 'tiny',
        'unsigned', TRUE
      ]);

    $fields['payment_account'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Bank account'))
      ->setRequired(FALSE)
      ->setSettings([
        'target_type' => 'site_payments_account',
      ]);

    $fields['payment_plugin'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('ID of the payment plugin'))
      ->setRequired(FALSE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ]);

    $fields['payment_system'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('ID of the payment system'))
      ->setDescription(new TranslatableMarkup('ID of the payment system in 1C, etc.'))
      ->setRequired(FALSE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ]);

    $fields['payment_merchant_id'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('ID cash'))
      ->setRequired(FALSE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ]);

    $fields['payment_id'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Payment ID'))
      ->setRequired(FALSE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ]);

    $fields['payment_link'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('URL of the payment link'))
      ->setRequired(FALSE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 2083,
      ]);

    $fields['payment_status'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Payment status'))
      ->setRequired(FALSE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ]);

    $fields['payment_type'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Transaction type'))
      ->setRequired(FALSE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ]);

    $fields['order_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Order ID'))
      ->setRequired(FALSE)
      ->setSettings([
        'size' => 'normal',
        'unsigned', TRUE,
      ]);

    $fields['price'] = BaseFieldDefinition::create('site_commerce_price')
      ->setLabel(new TranslatableMarkup('Cost and currency code'))
      ->setRequired(TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Description'))
      ->setDescription(new TranslatableMarkup('Brief description of the purpose of the transaction.'));

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(new TranslatableMarkup('Data'))
      ->setDescription(new TranslatableMarkup('A serialized array of additional data.'))
      ->setRequired(FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('Date the transaction was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('Date of the last data update.'));

    $fields['overdue'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Date when the link to the payment went bad.'))
      ->setRequired(FALSE);

    $fields['webhook'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Date when data was received via webhook.'))
      ->setRequired(FALSE);

    $fields['finance_system'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Date of sending data to the finance system (for example, 1C).'))
      ->setRequired(FALSE);

    $fields['viewed'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('The date the user viewed information about the transaction.'))
      ->setRequired(FALSE);

    $fields['batch'] = BaseFieldDefinition::create('boolean')
    ->setLabel(new TranslatableMarkup('Status batch operation'))
    ->setRequired(FALSE);

    $fields['hostname'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('IP-address'))
      ->setRequired(TRUE)
      ->setSettings([
        'type' => 'varchar_ascii',
        'max_length' => 128,
      ]);

    return $fields;
  }
}
