<?php

namespace Drupal\site_payments\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\site_payments\ReceiptInterface;
use Drupal\site_payments\TransactionInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the Receipt entity.
 *
 * @ingroup site_payments
 *
 * @ContentEntityType(
 *   id = "site_payments_receipt",
 *   label = @Translation("Receipt"),
 *   base_table = "site_payments_receipt",
 *   admin_permission = "administer site_payments_receipt",
 *   fieldable = FALSE,
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "receipt_id",
 *     "uuid" = "uuid",
 *     "owner" = "uid"
 *   }
 * )
 */
class Receipt extends ContentEntityBase implements ReceiptInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return (int) $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp): ReceiptInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getData(): array|null {
    $data = NULL;
    $value = $this->get('data')->getValue();
    if (!empty($value)) {
      $data = reset($value) ?: NULL;
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function setData(array $data): ReceiptInterface {
    $this->set('data', $data);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTransaction(): TransactionInterface {
    return $this->get('transaction_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string {
    return (string) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus(string $status): ReceiptInterface {
    $status = mb_strtoupper($status);
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['uid']
      ->setLabel(t('User ID'));

    $fields['transaction_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Transaction ID'))
      ->setSettings([
        'target_type' => 'site_payments_transaction',
      ])
      ->setRequired(TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('A serialized array of additional data.'))
      ->setRequired(FALSE);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setRequired(TRUE)
      ->setDefaultValue('NEW');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('Date the item was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('Date the item was edited.'));

    return $fields;
  }

}
