<?php

namespace Drupal\site_payments;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Transaction entities.
 *
 * @ingroup site_payments
 */
interface TransactionInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the transaction customer status.
   *
   * @return bool
   *   Customer status TRUE if is new user.
   */
  public function isNewClient();

  /**
   * Gets the transaction cost value.
   *
   * @param bool $format
   *   If TRUE will be remove trailing zeros.
   *
   * @return float
   */
  public function getPrice(bool $format = FALSE);

  /**
   * Set the transaction price value.
   *
   * @param float $value
   *   The transaction price value.
   * @param string $currency_code
   *   The currency code.
   *
   * @return \Drupal\site_payments\Entity\TransactionInterface
   *   The called entity.
   */
  public function setPrice(float $value, string $currency_code = NULL);

  /**
   * Gets the currency code of transaction.
   *
   * @return string
   */
  public function getPriceСurrencyСode();

  /**
   * Gets ID of payment plugin.
   *
   * @return string
   */
  public function getPaymentPluginId();

  /**
   * Gets payment system id.
   *
   * @return string
   */
  public function getPaymentSystemId();

  /**
   * Gets ID of payment.
   *
   * @return string
   */
  public function getPaymentId();

  /**
   * Sets ID of payment.
   *
   * @param string|null $payment_id
   *   The transaction payment id.
   *
   * @return \Drupal\site_payments\Entity\TransactionInterface
   *   The called entity.
   */
  public function setPaymentId(?string $payment_id = NULL);

  /**
   * Gets type of payment.
   *
   * @return string
   */
  public function getPaymentType();

  /**
   * Gets ID of payment account.
   *
   * @return int
   */
  public function getPaymentAccountId();

  /**
   * Gets payment account.
   *
   * @return \Drupal\site_payments\AccountInterface
   *   The payment account entity.
   */
  public function getPaymentAccount();

  /**
   * Gets description.
   *
   * @return string
   */
  public function getDescription();

  /**
   * Sets description.
   *
   * @param string $description
   *
   * @return string
   */
  public function setDescription(string $description);

  /**
   * Gets the transaction creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Transaction.
   */
  public function getCreatedTime();

  /**
   * Sets the transaction creation timestamp.
   *
   * @param int $timestamp
   *   The transaction creation timestamp.
   *
   * @return \Drupal\site_payments\Entity\TransactionInterface
   *   The called entity.
   */
  public function setCreatedTime(int $timestamp);

  /**
   * Gets the transaction overdue timestamp.
   *
   * @return int
   *   Creation timestamp of the Transaction.
   */
  public function getOverdueTime();

  /**
   * Sets the transaction overdue timestamp.
   *
   * @param int $timestamp
   *   The transaction creation timestamp.
   *
   * @return \Drupal\site_payments\Entity\TransactionInterface
   *   The called entity.
   */
  public function setOverdueTime(int $timestamp);

  /**
   * Gets the payment link status.
   *
   * @param int $request_time
   *   The timestamp for checking. If NULL $request_time is current time.
   * @return bool
   *   TRUE if the payment link is overdue.
   */
  public function isPaymentOverdue(?int $request_time = NULL);

  /**
   * Gets the transaction payment status.
   *
   * @return string
   *   The payment status.
   */
  public function getPaymentStatus();

  /**
   * Sets the transaction payment status.
   *
   * @param string $status
   *   The transaction payment status.
   *
   * @return \Drupal\site_payments\Entity\TransactionInterface
   *   The called entity.
   */
  public function setPaymentStatus(string $payment_status);

  /**
   * Checks whether the transaction is paid or not.
   *
   * @return bool
   *   If TRUE transaction is paid.
   */
  public function isPaid();

  /**
   * Gets the transaction payment link.
   *
   * @return string
   *   The payment link.
   */
  public function getPaymentLink();

  /**
   * Sets the transaction payment link.
   *
   * @param string|null $payment_link
   *   The transaction payment link.
   *
   * @return \Drupal\site_payments\Entity\TransactionInterface
   *   The called entity.
   */
  public function setPaymentLink(?string $payment_link = NULL);

  /**
   * Gets the finance system status.
   *
   * @return bool
   *   The finance system status. TRUE if data send to finance system.
   */
  public function getFinanceSystemStatus();

  /**
   * Sets the webhook status.
   *
   * @param bool $status
   *   The webhook status. TRUE if data received from webhook.
   *
   * @return \Drupal\site_payments\Entity\TransactionInterface
   *   The called entity.
   */
  public function setWebhookStatus(bool $status = TRUE);

  /**
   * Gets the webhook status.
   *
   * @return bool
   *   The webhook status. TRUE if data received from webhook.
   */
  public function getWebhookStatus();

  /**
   * Sets the finance system status.
   *
   * @param bool $status
   *   The finance system status. TRUE if data send to finance system.
   *
   * @return \Drupal\site_payments\Entity\TransactionInterface
   *   The called entity.
   */
  public function setFinanceSystemStatus(bool $status = TRUE);

  /**
   * Gets the transaction viewed timestamp.
   *
   * @return int
   *   The transaction viewed timestamp.
   */
  public function getViewedTime();

  /**
   * Sets the transaction viewed timestamp.
   *
   * @param int $timestamp
   *   The transaction viewed timestamp.
   *
   * @return \Drupal\site_payments\Entity\TransactionInterface
   *   The called entity.
   */
  public function setViewedTime(int $timestamp);

  /**
   * Gets additional data of transaction.
   *
   * @return array|null
   */
  public function getData();

  /**
   * Sets additional data of transaction.
   *
   * @param array $data
   *   The additional data of transaction.
   *
   * @return \Drupal\site_payments\Entity\TransactionInterface
   *   The called entity.
   */
  public function setData(array $data);

  /**
   * Gets the transaction order id.
   *
   * @return int
   *   The transaction order id.
   */
  public function getOrderId();

  /**
   * Sets the transaction creation timestamp.
   *
   * @param int $timestamp
   *   The transaction creation timestamp.
   *
   * @return \Drupal\site_payments\Entity\TransactionInterface
   *   The called entity.
   */
  public function setOrderId(int $order_id);

}
