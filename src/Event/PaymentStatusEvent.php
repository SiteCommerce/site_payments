<?php

namespace Drupal\site_payments\Event;

use Drupal\site_payments\TransactionInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Events while change payment status.
 */
class PaymentStatusEvent extends Event {

  /**
   * Events before check payment status.
   */
  const CHECK_PAYMENT_STATUS_BEFORE = 'site_payments.check_payment_status.before';

  /**
   * Events after check payment status.
   */
  const CHECK_PAYMENT_STATUS_AFTER = 'site_payments.check_payment_status.after';

  /**
   * Events if order is paid.
   */
  const PAYMENT_STATUS_PAID = 'site_payments.payment_status.paid';

  /**
   * @param TransactionInterface $transaction
   *   Transaction entity.
   */
  protected $transaction;

  /**
   * @param array $data
   *   Data for check payments and results.
   */
  protected $data;

  /**
   * PaymentStatusEvent constructor.
   */
  public function __construct(TransactionInterface $transaction, array $data) {
    $this->transaction = $transaction;
    $this->data = $data;
  }

  /**
   * Gets transaction entity.
   */
  public function getTransaction(): TransactionInterface {
    return $this->transaction;
  }

  /**
   * Gets data.
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * Sets transaction entity.
   */
  public function setTransaction(TransactionInterface $transaction): PaymentStatusEvent {
    $this->transaction = $transaction;
    return $this;
  }

  /**
   * Gets data.
   */
  public function setData(array $data): PaymentStatusEvent {
    $this->data = $data;
    return $this;
  }
}
