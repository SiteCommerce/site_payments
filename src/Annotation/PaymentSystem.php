<?php

namespace Drupal\site_payments\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Плагин PaymentSystem для подключения платежных систем.
 *
 * @Annotation
 */
class PaymentSystem extends Plugin {

  /**
   * ID плагина.
   */
  public $id;

  /**
   * Название плагина.
   */
  public $label;

  /**
   * Название способа оплаты на странице оформления заказа.
   */
  public $payment_method_name;

}
