<?php

namespace Drupal\site_payments\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\site_payments\TransactionInterface;

class PaymentFailureController extends ControllerBase {

  /**
   * Страница с уведомлением после не удачной оплаты.
   */
  public function paymentFailure(string $uuid) {
    $noindex_meta_tag = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex, nofollow',
      ],
    ];

    $transactions = $this->entityTypeManager()->getStorage('site_payments_transaction')->loadByProperties(['uuid' => $uuid]);
    $transaction = reset($transactions);
    if ($transaction instanceof TransactionInterface) {
      $transaction->setWebhookStatus();
      $transaction->setPaymentStatus('failure');
      $transaction->save();
    }

    $data = [];

    return [
      '#theme' => 'site_payments_payment_failure',
      '#data' => $data,
      '#attached' => [
        'library' => [],
        'html_head' => [[$noindex_meta_tag, 'noindex']],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
