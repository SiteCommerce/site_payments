<?php

namespace Drupal\site_payments\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\site_payments\TransactionInterface;

class PaymentSuccessController extends ControllerBase {

  /**
   * Страница с уведомлением после успешной оплаты.
   */
  public function paymentSuccess(string $uuid) {
    $noindex_meta_tag = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex, nofollow',
      ],
    ];

    $data = [
      'success' => FALSE,
    ];

    // TODO: Данный способ обновления статуса оплаты необходимо реализовывать через вызов webhook,
    // т.к. текущая реализация дает сбои в случае если пользователь оплатил заказ но закрыл браузер или не произошел редирект.
    // В дальнейшем вызов данного метода будет игнорироваться в зависимости от настроек
    // способа проверки статуса оплаты.
    // Выполняем вызов плагина платежной системы, в которой совершалась транзакция.
    // В зависимости от платежной системы структура данных, которая получена от сервера платежной системы может отличаться.
    // Получаем транзакцию по номеру.
    $transactions = $this->entityTypeManager()->getStorage('site_payments_transaction')->loadByProperties(['uuid' => $uuid]);
    $transaction = $transactions ? reset($transactions) : NULL;
    if ($transaction instanceof TransactionInterface) {
      // Сохраняем в транзакцию полученные от платежного шлюза параметры.
      $params = \Drupal::request()->query->all();
      if (!empty($params)) {
        $transaction_data = $transaction->getData();
        $transaction_data['payment_system'] = $params;
        $transaction->setData($transaction_data);
        $transaction->save();
      }

      $plugin_service = \Drupal::service('plugin.manager.site_payments.payment_system');
      $payment_plugin_id = $transaction->getPaymentPluginId();
      if ($plugin_service->hasDefinition($payment_plugin_id)) {
        // Инициализация плагина оплаты.
        $plugin_instance = $plugin_service->createInstance($payment_plugin_id);

        // Проверяем статус оплаты.
        $payment_status = $plugin_instance->checkPaymentStatus($transaction);
        if ($payment_status && $payment_status == 'paid') {
          $transaction->setPaymentStatus($payment_status);
          $data = [
            'success' => TRUE,
          ];
        }
      }

      $transaction->setWebhookStatus();
      $transaction->save();
    }

    return [
      '#theme' => 'site_payments_payment_success',
      '#data' => $data,
      '#attached' => [
        'library' => [],
        'html_head' => [[$noindex_meta_tag, 'noindex']],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
